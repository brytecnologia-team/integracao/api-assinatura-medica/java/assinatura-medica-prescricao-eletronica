# Geração de Assinatura Médica

Este é um exemplo integração dos serviços da API de assinatura com clientes baseados em tecnologia Java.

Este exemplo apresenta os passos necessários para a geração de metadados referentes ao médico na prescrição médica.

  - Passo 1: Criação do formulário com o documento que será assinado.
  - Passo 2: Recebimento do documento com os metadados.
  - Passo 3: O documento recebido deverá ser enviado para uma assinatura do tipo PDF/PADES.

### Assinatura PDF

Após ter os metadados inseridos, o documento deverá ser enviado novamente ao HUB, para que seja realizada de fato a assinatura. Um Exemplo de como realizar a assinatura se encontra [aqui](https://gitlab.com/brytecnologia-team/integracao/api-assinatura/java/backend-assinatura-pdf-com-bry-extension).


### Tech

O exemplo utiliza das bibliotecas Java abaixo:
* [SpringBoot Web] - Spring RestTemplate for create requests
* [JDK 8] - Java 8

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

| Variável  |                   Descrição                   | Classe de configuração |  
| --------- | --------------------------------------------- | ----------------------- |
| token | Access Token para o consumo do serviço (JWT). |     AssinaturamedicaApplication.java    | 

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a produção da assinatura.

### Uso

Para execução da aplicação de exemplo, compile diretamente as classes ou importe o projeto em sua IDE de preferência.
Utilizamos o JRE versão 8 para desenvolvimento e execução.

[SpringBoot Web]: <https://spring.io/>
[JDK 8]: <https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html>